# Обьект - сущность, обьеденяющая данные и методы для работы с ними (состояние и поведение)
# Чертеж это класс, а обьект это дом
# класс - это новый тип данных, а обьект - его конкретный представитель
# у любого обьекта есть id и тип


class Cat:
    def meow(self):
        print(self)
        print('Мяу')


tom = Cat()
# int
murka = Cat()

print(tom is murka)
print(tom == murka)
print(tom)
print(murka)
print(type(tom))
print(type(1))

tom.meow()
Cat.meow(tom)
murka.meow()


class Cat:
    def __init__(self, name, age):
        self.name = name
        self.age = age
        new = 'some text'

    def meow(self):
        # print(new)
        print(f'{self.name} говорит: Мяу')


tom = Cat('Том', 5)
murka = Cat('Мурка', 3)
tom.meow()
murka.meow()
print(tom.name)
print(tom.age)

import cat
tom = cat
tom.meow()
print(tom.name)
print(tom.age)
cat.name = 'Мурка'
print(tom.name)
