class Car:
    def __init__(self, brand, model, color):
        self.brand = brand
        self.model = model
        self.color = color
        self.wheels = 4
        self.seats = 5
        self.doors = 5
        self.speed = 220

    def set_brand(self, brand):
        self.brand = brand

    def set_model(self, model):
        self.model = model

    def start(self):
        print(f"Наш {self.brand} {self.model} делает пых-пых-пых и наконец-то заводится")

    def drive(self, destination, distance):
        print(f"Наш {self.brand} {self.model} едет в {destination} {distance} км")


my_car = Car('Жигуль', '2109', 'Вишневый')
my_car.start()
my_car.drive('Жмеринка', 200)


class Petrol(Car):
    def __init__(self, brand, model, color):
        super().__init__(brand, model, color)
        self.fuel = 'Бензин'
        self.unit = 'л'
        self.consumption = 10


class Diesel(Car):
    def __init__(self, brand, model, color):
        super().__init__(brand, model, color)
        self.fuel = 'Соляра'
        self.unit = 'л'
        self.consumption = 12


class Electric(Car):
    def __init__(self, brand, model, color):
        super().__init__(brand, model, color)
        self.fuel = 'Электричество'
        self.unit = '%'
        self.consumption = 20


class Passenger:
    @staticmethod
    def set_passengers(quantity):
        print(f"В наш тазик влезло {quantity} человек")


class Jeep(Diesel, Passenger):
    def __init__(self, brand, model, color):
        super().__init__(brand, model, color)
        self.seats = 7
        self.speed = 180

    def drive(self, destination, distance):
        print(f"Наш {self.brand} {self.model} едет в {destination} {distance} км")
        print(f"Нам надо заправить: {self.fuel} {round(self.consumption/100 * distance, 2)} л")


my_jeep = Jeep('Infiniti', 'QX90', 'Черный')
my_jeep.start()
my_jeep.drive('Киев', 480)
my_jeep.set_passengers(4)

