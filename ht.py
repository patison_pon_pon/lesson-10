import os


def multiplication_table(start_value, end_value):
    try:
        for i in range(start_value, end_value + 1):
            for j in range(start_value, end_value + 1):
                # for c in range(start_value, end_value + 1):
                if not os.path.isdir(f"home_task/dir_{c}"):
                    os.makedirs(f"home_task/dir_{c}")
                with open(f"home_task/dir_{c}/table_{c}.txt", 'w') as my_folder:
                    for c in range(start_value, end_value + 1):
                        result = (str(i) + "*" + str(j) + "=" + str(i * j))
                        my_folder.write(result)
    except Exception as e:
        print(f'Ошибка!{e}')


multiplication_table(1, 5)
